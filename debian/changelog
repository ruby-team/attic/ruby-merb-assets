ruby-merb-assets (1.1.3-3) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Remove version in the gem2deb build-dependency
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 05:59:33 +0530

ruby-merb-assets (1.1.3-2) unstable; urgency=low

  * Team upload
  * debian/control:
    + remove obsolete DM-Upload-Allowed flag
    + use canonical URI in Vcs-* fields
    + bump Standards-Version to 3.9.4 (no changes needed)
    + drop transitional packages
  * debian/copyright: use copyright-format/1.0 official URL for Format
  * Use the rake method to run the tests.
    They now can find spec_helper.rb (Closes: #725544)

 -- Cédric Boutillier <boutil@debian.org>  Fri, 18 Oct 2013 23:55:10 +0200

ruby-merb-assets (1.1.3-1) unstable; urgency=low

  * Convert to new-style ruby package

 -- Tollef Fog Heen <tfheen@debian.org>  Fri, 18 May 2012 12:53:46 +0200

merb (1.0.12+dfsg-6) unstable; urgency=low

  * Convert to new-style packaging.

 -- Tollef Fog Heen <tfheen@debian.org>  Sat, 21 Apr 2012 19:10:56 +0200

merb (1.0.12+dfsg-5) unstable; urgency=low

  * Use dep.to_spec.activate instead of Gem.activate, since newer rubygems
    doesn't support Gem.activate any longer.

 -- Tollef Fog Heen <tfheen@debian.org>  Mon, 16 Apr 2012 22:00:50 +0200

merb (1.0.12+dfsg-4) unstable; urgency=low

  * Add merb-param-protection package.
  * Merb depends on RubyGems to enable library dependency plugin mechanism.

 -- Joshua Timberman <joshua@opscode.com>  Wed, 17 Mar 2010 19:38:55 -0600

merb (1.0.12+dfsg-3) unstable; urgency=low

  [ Paul van Tilburg ]
  * debian/watch: changed the regexp so that it makes PET happy too.

  [ Mike Castleman ]
  * debian/copyright: clarify that we don't ship the allison library
    (closes: #574142)

 -- Mike Castleman <m@mlcastle.net>  Wed, 17 Mar 2010 03:07:17 -0400

merb (1.0.12+dfsg-2) unstable; urgency=low

   * Packaging approved for Ubuntu, updating for Debian.
   * Rename merb-slices binary from slice to merb-slice.
   * Repack upstream tarball, should not affect Debian packaging but noted:
     + Replaced upstream empty LICENSE file with MIT license
     + Removed object files which are part of the webrat spec testing.

 -- Joshua Timberman <joshua@opscode.com>  Thu, 27 Aug 2009 08:56:09 -0600

merb (1.0.12+dfsg-1) unstable; urgency=low

   * Initial release (Closes: #538928)

 -- Joshua Timberman <joshua@opscode.com>  Mon, 27 Jul 2009 23:38:37 -0600
